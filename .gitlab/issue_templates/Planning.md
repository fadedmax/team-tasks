# Capacity & Planning

List individual PTO entries for the current milestone

## Links

- [Memory Team Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=group%3A%3Amemory)
- [Memory Team Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Amemory&label_name[]=Roadmap)

### Priorities for `%milestone`

-

### Stretch Goals

- 

/label ~"group::memory" ~"devops::enablement"
/assign @craig-gomes @fzimmer

**Reminder** 
Set ~"Deliverable" as the plan for the milestone solidifies.  Review during the milestone

cc @gl-memory @cdu1 @ankelly